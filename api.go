package poloniex

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"github.com/pkg/errors"

	"gopkg.in/beatgammit/turnpike.v2"
)

type (
	//Poloniex describes the API
	Poloniex struct {
		Key          string
		Secret       string
		ws           *turnpike.Client
		subscribedTo map[string]bool
		debug        bool
		nonce        int64
		mutex        sync.Mutex
	}
)

const (
	// PUBLICURI is the address of the public API on Poloniex
	PUBLICURI = "https://poloniex.com/public"
	// PRIVATEURI is the address of the public API on Poloniex
	PRIVATEURI = "https://poloniex.com/tradingApi"
)

func (p *Poloniex) InitWS() {
	if p.ws != nil {
		return
	}
	err := retry(100, 3*time.Second, func() error {
		t := &tls.Config{InsecureSkipVerify: true}
		u := "wss://api.poloniex.com"
		c, err := turnpike.NewWebsocketClient(turnpike.JSON, u, t)
		if err != nil {
			log.Println(err)
			return errors.Wrap(err, "open of websocket connection to "+u+" failed")
		}
		_, err = c.JoinRealm("realm1", nil)
		if err != nil {
			log.Println(err)
			return errors.Wrap(err, "joining realm1 failed")
		}
		p.ws = c
		return nil
	})
	if err != nil {
		log.Fatalln(errors.Wrap(err, "retries exhausted, fatal."))
	}
	p.subscribedTo = map[string]bool{}

}

func retry(attempts int, sleep time.Duration, callback func() error) (err error) {
	for i := 0; ; i++ {
		err = callback()
		if err == nil {
			return
		}

		if i >= (attempts - 1) {
			break
		}

		time.Sleep(sleep)

		log.Println("retrying after error:", err)
	}
	return fmt.Errorf("after %d attempts, last error: %s", attempts, err)
}

func (p *Poloniex) isSubscribed(code string) bool {
	ok := p.subscribedTo[code]
	return ok
}

func (p *Poloniex) Debug() {
	p.debug = true
}

func (p *Poloniex) GetNonce() string {
	p.nonce++
	return fmt.Sprintf("%d", p.nonce)
}

func New(configfile string) *Poloniex {
	p := &Poloniex{}
	b, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.Fatalln(errors.Wrap(err, "reading "+configfile+" failed."))
	}
	err = json.Unmarshal(b, p)
	if err != nil {
		log.Fatalln(errors.Wrap(err, "unmarshal of config failed."))
	}
	p.nonce = time.Now().UnixNano()
	p.mutex = sync.Mutex{}
	return p
}

func trace(s string) (string, time.Time) {
	return s, time.Now()
}

func un(s string, startTime time.Time) {
	elapsed := time.Since(startTime)
	log.Printf("trace end: %s, elapsed %f secs\n", s, elapsed.Seconds())
}
