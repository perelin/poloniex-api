package poloniex

import "strconv"

type (
	WSTicker struct {
		Pair          string
		Last          float64
		Ask           float64
		Bid           float64
		PercentChange float64
		BaseVolume    float64
		QuoteVolume   float64
		IsFrozen      bool
		DailyHigh     float64
		DailyLow      float64
	}

	WSOrder struct {
		Rate   float64 `json:"rate,string"`
		Type   string  `json:"type"`
		Amount float64 `json:"amount,string"`
	}
	WSTrade struct {
		TradeID string
		Rate    float64 `json:",string"`
		Amount  float64 `json:",string"`
		Type    string
		Date    string
	}
	WSOrderOrTrade []struct {
		Data map[string]string
		Type string
	}
)

func f(i interface{}) float64 {
	switch i := i.(type) {
	case string:
		a, err := strconv.ParseFloat(i, 64)
		if err != nil {
			return 0.0
		}
		return a
	case float64:
		return i
	case int64:
		return float64(i)
	}
	return 0.0
}
